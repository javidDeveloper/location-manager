package manager.location.s.javid.ir.locationmanager.Models;

import java.util.List;

/**
 * Developed by javid
 * Project : book
 */
public class LoctionsStatus {
    private List<LoctionStatus> loctionStatuses;

    public LoctionsStatus(List<LoctionStatus> loctionStatuses) {
        this.loctionStatuses = loctionStatuses;
    }

    public List<LoctionStatus> getLoctionStatuses() {
        return loctionStatuses;
    }

    public void setLoctionStatuses(List<LoctionStatus> loctionStatuses) {
        this.loctionStatuses = loctionStatuses;
    }
}
