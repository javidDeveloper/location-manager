package manager.location.s.javid.ir.locationmanager.Network;


import java.util.List;

import manager.location.s.javid.ir.locationmanager.Models.LoctionStatus;
import manager.location.s.javid.ir.locationmanager.Models.StatusRest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebServices {

    @GET(Api.BASE_URL + Api.WEB_SERVICE + Api.GET_LOCATION)
    Call<List<LoctionStatus>> getLocation(@Query("imei") String imei);

}

