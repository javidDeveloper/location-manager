package manager.location.s.javid.ir.locationmanager.Sections;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import manager.location.s.javid.ir.locationmanager.Models.LoctionStatus;
import manager.location.s.javid.ir.locationmanager.Models.LoctionsStatus;
import manager.location.s.javid.ir.locationmanager.R;
import manager.location.s.javid.ir.locationmanager.Tools.Utility;

/**
 * Developed by javid
 * Project : location manager
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {
    private List<LoctionStatus> list;
    private Context context;
    private View view;
    private OnClickItem onClickItem;
    private int positionSelect = -1;

    public LocationAdapter(List<LoctionStatus> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    @Override
    public LocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.location_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LocationAdapter.ViewHolder holder, final int position) {
        LoctionStatus loctionStatus = list.get(position);
        if (position == positionSelect)
            holder.selected();
        else
            holder.unSelected();
        holder.txtTime.setText(Utility.convertTimeToPersian(loctionStatus.getTime()));
        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickItem.setOnClick(position);
                positionSelect = position;
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTime;
        CardView llItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTime = itemView.findViewById(R.id.txt_time);
            llItem = itemView.findViewById(R.id.ll_item);
        }

        public void selected() {
            llItem.setBackgroundColor(context.getResources().getColor(R.color.select_color));
        }

        public void unSelected() {
            llItem.setBackgroundColor(context.getResources().getColor(R.color.un_select_color));
        }
    }

    public interface OnClickItem {
        void setOnClick(int position);
    }
}
