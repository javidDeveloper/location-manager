package manager.location.s.javid.ir.locationmanager.Models;

/**
 * Developed by javid
 * Project : book
 */
public class StatusRest {
    private String imeiCode;
    private String status;

    public StatusRest() {
    }

    public String getImeiCode() {
        return imeiCode;
    }

    public void setImeiCode(String imeiCode) {
        this.imeiCode = imeiCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}


