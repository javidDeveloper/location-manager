package manager.location.s.javid.ir.locationmanager.Tools;

/**
 * Developed by javid
 * Project : location manager
 */
public class Utility {
    public static String convertNumbersToPersian(String text) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (((int) c) >= 48 && ((int) c) < 58) {
                c = (char) ((c - '0') + 0x0660);
            }
            result.append(c);
        }
        return result.toString();
    }

    public static String convertTimeToPersian(String dataTime) {
        JalaliCalendar.YearMonthDate yearMonthDate;
        int year;
        int month;
        int day;
        String hour = "";
        String min = "";
        String[] date;
        String[] time;
        String finalDate = "";
        //2019-05-24 21:07:41
        date = dataTime.split("-");
        year = Integer.parseInt(date[0]);
        month = Integer.parseInt(date[1]);
        String[] space = date[2].split(" ");
        day = Integer.parseInt(space[0]);
        yearMonthDate = JalaliCalendar.gregorianToJalali(new JalaliCalendar.YearMonthDate(year, month, day));
        time = space[1].split(":");
        finalDate = time[0] + ":" + time[1] + " " + yearMonthDate.toString();
        return convertNumbersToPersian(finalDate);
    }

}
