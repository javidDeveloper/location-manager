package manager.location.s.javid.ir.locationmanager;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import manager.location.s.javid.ir.locationmanager.Managers.NetworkManager;
import manager.location.s.javid.ir.locationmanager.Models.ErrorRest;
import manager.location.s.javid.ir.locationmanager.Models.LoctionStatus;
import manager.location.s.javid.ir.locationmanager.Models.LoctionsStatus;
import manager.location.s.javid.ir.locationmanager.Models.StatusRest;
import manager.location.s.javid.ir.locationmanager.Network.RestConnection;
import manager.location.s.javid.ir.locationmanager.Sections.LocationAdapter;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, Observer {
    private RecyclerView recyclerLocations;
    private RecyclerView.LayoutManager layoutManager;
    private LocationAdapter locationAdapter;
    private GoogleMap mMap;
    private ImageView imgRefresh;
    private TextView txtErrorConnect;
    private Context mContext = this;
    private List<LoctionStatus> statusList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        NetworkManager.getInstance().addObserver(this);
        final LoctionStatus loctionStatus = new LoctionStatus("");
        NetworkManager.getInstance().callLocation(loctionStatus);
        initView();
        layoutManager = new LinearLayoutManager(mContext, LinearLayout.HORIZONTAL, false);
        recyclerLocations.setLayoutManager(layoutManager);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        imgRefresh.setVisibility(View.GONE);
        txtErrorConnect.setVisibility(View.GONE);
        recyclerLocations.setVisibility(View.VISIBLE);
        imgRefresh.setOnClickListener(view -> {
            imgRefresh.setVisibility(View.GONE);
            txtErrorConnect.setVisibility(View.GONE);
            recyclerLocations.setVisibility(View.VISIBLE);
            NetworkManager.getInstance().callLocation(loctionStatus);
        });
        txtErrorConnect.setOnClickListener((View v) -> {

        });
    }

    private void initView() {
        recyclerLocations = findViewById(R.id.recycler_locations);
        imgRefresh = findViewById(R.id.img_refresh);
        txtErrorConnect = findViewById(R.id.txt_error_connect);
    }

    private void initAdapter(final LoctionsStatus list) {
        imgRefresh.setVisibility(View.GONE);
        txtErrorConnect.setVisibility(View.GONE);
        recyclerLocations.setVisibility(View.VISIBLE);
        locationAdapter = new LocationAdapter(list.getLoctionStatuses(), mContext);
        locationAdapter.setOnClickItem(position -> {
            mMap.resetMinMaxZoomPreference();
            LatLng sydney = new LatLng(Double.parseDouble(list.getLoctionStatuses().get(position).getLatitude()), Double.parseDouble(list.getLoctionStatuses().get(position).getLongitude()));
            mMap.addMarker(new MarkerOptions().position(sydney).title(list.getLoctionStatuses().get(position).getTime()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            zoomCamera(Double.parseDouble(list.getLoctionStatuses().get(position).getLatitude()), Double.parseDouble(list.getLoctionStatuses().get(position).getLongitude()));
        });
        recyclerLocations.setAdapter(locationAdapter);
    }

    private void zoomCamera(double lat, double lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void initMap(LoctionsStatus o) {
        for (LoctionStatus loctionStatus : o.getLoctionStatuses()) {
            LatLng sydney = new LatLng(Double.parseDouble(loctionStatus.getLatitude()), Double.parseDouble(loctionStatus.getLongitude()));
            mMap.addMarker(new MarkerOptions().position(sydney).title(loctionStatus.getTime()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

    }

    @Override
    public void update(Observable observable, final Object o) {
        if (observable instanceof NetworkManager) {
            if (o instanceof LoctionsStatus) {
                statusList = ((LoctionsStatus) o).getLoctionStatuses();
                initAdapter((LoctionsStatus) o);
                initMap((LoctionsStatus) o);
            } else if (o instanceof StatusRest) {

            } else if (o instanceof ErrorRest) {
                runOnUiThread(() -> {
                    Toast.makeText(mContext, ((ErrorRest) o).getStatue(), Toast.LENGTH_SHORT).show();
                    imgRefresh.setVisibility(View.VISIBLE);
                    recyclerLocations.setVisibility(View.GONE);
                    txtErrorConnect.setVisibility(View.VISIBLE);

                });
            }
        }
    }

}
