package manager.location.s.javid.ir.locationmanager.Network;

import java.util.List;
import java.util.Observable;

import manager.location.s.javid.ir.locationmanager.Models.ErrorRest;
import manager.location.s.javid.ir.locationmanager.Models.LoctionStatus;
import manager.location.s.javid.ir.locationmanager.Models.LoctionsStatus;
import manager.location.s.javid.ir.locationmanager.Models.StatusRest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static manager.location.s.javid.ir.locationmanager.Models.ErrorRest.ERROR_MESSAGE;


public class RestConnection extends Observable {
    public static RestConnection instance;

    public RestConnection() {
    }

    public static RestConnection getInstance() {
        if (instance == null)
            instance = new RestConnection();
        return instance;
    }

    public void callLocation(LoctionStatus loctionStatus) {

        Api.getClient()
                .create(WebServices.class)
                .getLocation(loctionStatus.getImeiCode())
                .enqueue(new Callback<List<LoctionStatus>>() {
                    @Override
                    public void onResponse(Call<List<LoctionStatus>> call, Response<List<LoctionStatus>> response) {
                        if (response.isSuccessful()) {
                            LoctionsStatus statusRest = new LoctionsStatus(response.body());
                            setChanged();
                            notifyObservers(statusRest);
                        } else {
                            ErrorRest errorRest = new ErrorRest(ERROR_MESSAGE, response.message());
                            setChanged();
                            notifyObservers(errorRest);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<LoctionStatus>> call, Throwable t) {
                        ErrorRest errorRest = new ErrorRest(ERROR_MESSAGE, t.toString());
                        setChanged();
                        notifyObservers(errorRest);
                    }
                });
    }
}