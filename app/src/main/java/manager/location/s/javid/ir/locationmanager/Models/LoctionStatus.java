package manager.location.s.javid.ir.locationmanager.Models;

/**
 * Developed by javid
 * Project : book
 */
public class LoctionStatus {
    //    private String name;
    private String imeiCode;
    private String latitude;
    private String longitude;
    private String time;

    public LoctionStatus() {
    }

    public LoctionStatus(String imeiCode) {
        this.imeiCode = imeiCode;
    }

    public String getImeiCode() {
        return imeiCode;
    }

    public void setImeiCode(String imeiCode) {
        this.imeiCode = imeiCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
