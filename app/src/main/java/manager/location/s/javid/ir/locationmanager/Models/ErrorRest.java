package manager.location.s.javid.ir.locationmanager.Models;

/**
 * Developed by javid
 * Project : numberGenerator
 */
public class ErrorRest {
    public static final String ERROR_MESSAGE = "error.message";
    private String value;
    private String statue;

    public ErrorRest(String value, String statue) {
        this.value = value;
        this.statue = statue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatue() {
        return statue;
    }

    public void setStatue(String statue) {
        this.statue = statue;
    }
}
