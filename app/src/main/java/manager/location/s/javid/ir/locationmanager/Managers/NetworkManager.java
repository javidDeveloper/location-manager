package manager.location.s.javid.ir.locationmanager.Managers;

import java.util.Observable;
import java.util.Observer;

import manager.location.s.javid.ir.locationmanager.Models.ErrorRest;
import manager.location.s.javid.ir.locationmanager.Models.LoctionStatus;
import manager.location.s.javid.ir.locationmanager.Models.LoctionsStatus;
import manager.location.s.javid.ir.locationmanager.Models.StatusRest;
import manager.location.s.javid.ir.locationmanager.Network.RestConnection;

/**
 * Developed by javid
 */
public class NetworkManager extends Observable implements Observer {
    public static NetworkManager instance;
    private LoctionStatus oldLocationStatus;

    public static NetworkManager getInstance() {
        if (instance == null)
            instance = new NetworkManager();
        return instance;
    }

    public NetworkManager() {
        RestConnection.getInstance().addObserver(this);
    }

    public void callLocation(LoctionStatus loctionStatus) {
        RestConnection.getInstance().callLocation(loctionStatus);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof RestConnection) {
            if (o instanceof LoctionsStatus) {
                setChanged();
                notifyObservers(o);
            } else if (o instanceof StatusRest) {
                setChanged();
                notifyObservers(o);
            } else if (o instanceof ErrorRest) {
                setChanged();
                notifyObservers(o);
            }
        }
    }
}
